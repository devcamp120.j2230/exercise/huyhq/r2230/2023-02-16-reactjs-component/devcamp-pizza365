const { Component } = require("react");

class HeaderComponent extends Component {
    render() {
        return (
            < nav className="navbar navbar-expand-lg navbar-light bg-warning" >
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="row collapse navbar-collapse" id="navbarNav">
                    <ul className="row col-12 navbar-nav text-center text-dark">
                        <li className="nav-item active col-3">
                            <a className="nav-link" href="#">Trang chủ <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item col-3">
                            <a className="nav-link" href="#size-pizza-choose">Combo</a>
                        </li>
                        <li className="nav-item col-3">
                            <a className="nav-link" href="#type-pizza-choose">Loại Pizza</a>
                        </li>
                        <li className="nav-item col-3">
                            <a className="nav-link" href="#order-create">Gửi đơn hàng</a>
                        </li>
                    </ul>
                </div>
            </ nav>
        )
    }
};

export default HeaderComponent;
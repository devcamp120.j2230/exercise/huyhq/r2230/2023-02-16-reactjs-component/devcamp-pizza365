const { Component } = require("react");

class FooterComponent extends Component {
    render() {
        return (
            <div className="container-fluid mt-5 p-5 text-center" style={{ backgroundColor: "orange" }}>
                <div className="m-3"> <b>Footer</b> </div>
                <div className="m-3">
                    <a href="#" className="btn bg-dark text-light">
                        <i className="fa-solid fa-arrow-up"></i>
                        To the top
                    </a>
                </div>
                <div className="m-3">
                    <i className="fa-brands fa-facebook"></i>
                    <i className="fa-brands fa-youtube"></i>
                    <i className="fa-brands fa-twitter"></i>
                </div>
                <div className="m-3">
                    <p>Hoàng Quốc Huy</p>
                </div>
            </div>
        )
    }
};

export default FooterComponent;
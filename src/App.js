import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


import HeaderComponent from './header/HeaderComponent';
import FooterComponent from './footer/FooterComponent';
import ContentComponent from './content/ContentComponent';


function App() {
  return (
    <div className='container-fluid'>
      {/* <!-- NAVIGATION AREA --> */}
      <HeaderComponent />
      {/* <!-- BODY AREA --> */}
      <ContentComponent/>
      {/* <!-- FOOTER AREA --> */}
      <FooterComponent />
    </div>
  );
}

export default App;

import { Component } from "react";

class SizeComponent extends Component {
    render() {
        return (
            <div className="mt-5" id="size-pizza-choose">
                <div className="text-warning text-center mb-3">
                    <span className="h3 border-bottom border-warning">Chọn size pizza</span>
                    <p style={{ color: "orange" }}>Chọn combo pizza phù hợp với nhu cầu của bạn.</p>
                </div>
                <div className="row text-center justify-content-center">
                    <div className="row justify-content-center col-sm-12 col-md-4 mb-3 pl-3 pr-3 text-center">
                        <div className="card" style={{ width: "18rem" }}>
                            <div className="card-body p-0">
                                <h3 className="card-title p-3 size-pizza-title" style={{ backgroundColor: "orange" }}>S (small)
                                </h3>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>20cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>2</b></li>
                                    <li className="list-group-item">Salad: <b>200g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>2</b></li>
                                    <li className="list-group-item">
                                        <b className="h3">150.000</b>
                                        <p>VNĐ</p>
                                    </li>
                                    <li className="list-group-item bg-light">
                                        <button className="btn col-12 font-weight-bold size-pizza-button"
                                            data-is-selected-menu="N" id="small-choose"
                                            style={{ backgroundColor: "orange" }}>Chọn</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center col-sm-12 col-md-4 mb-3 pl-3 pr-3 text-center ">
                        <div className="card" style={{ width: "18rem" }}>
                            <div className="card-body p-0">
                                <h3 className="card-title p-3 size-pizza-title" style={{ backgroundColor: "orange" }}>M (medium)
                                </h3>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>25cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>4</b></li>
                                    <li className="list-group-item">Salad: <b>300g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>3</b></li>
                                    <li className="list-group-item">
                                        <b className="h3">200.000</b>
                                        <p>VNĐ</p>
                                    </li>
                                    <li className="list-group-item bg-light">
                                        <button className="btn col-12 font-weight-bold size-pizza-button"
                                            data-is-selected-menu="N" id="medium-choose"
                                            style={{ backgroundColor: "orange" }}>Chọn</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center col-sm-12 col-md-4 mb-3 pl-3 pr-3 text-center ">
                        <div className="card" style={{ width: "18rem" }}>
                            <div className="card-body p-0">
                                <h3 className="card-title p-3 size-pizza-title" style={{ backgroundColor: "orange" }}>L (large)
                                </h3>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">Đường kính: <b>30cm</b></li>
                                    <li className="list-group-item">Sườn nướng: <b>8</b></li>
                                    <li className="list-group-item">Salad: <b>500g</b></li>
                                    <li className="list-group-item">Nước ngọt: <b>4</b></li>
                                    <li className="list-group-item">
                                        <b className="h3">250.000</b>
                                        <p>VNĐ</p>
                                    </li>
                                    <li className="list-group-item bg-light">
                                        <button className="btn col-12 font-weight-bold size-pizza-button"
                                            data-is-selected-menu="N" id="large-choose"
                                            style={{ backgroundColor: "orange" }}>Chọn</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SizeComponent;
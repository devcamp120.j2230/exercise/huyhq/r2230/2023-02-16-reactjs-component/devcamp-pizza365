import { Component } from "react";

class FormComponent extends Component{
    render(){
        return(
            <div className="mt-5" id="order-create">
                <div className="text-warning text-center mb-3">
                    <span className="h3 border-bottom border-warning">Gửi đơn hàng</span>
                </div>
                <div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Họ Tên</label>
                        <div className="col-12">
                            <input type="text" id="inp-name" className="form-control" placeholder="Nhập tên"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Email</label>
                        <div className="col-12">
                            <input type="text" id="inp-email" className="form-control" placeholder="Nhập email"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Số điện thoại</label>
                        <div className="col-12">
                            <input type="number" id="inp-phone" className="form-control" placeholder="Nhập số điện thoại"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Địa chỉ</label>
                        <div className="col-12">
                            <input type="text" id="inp-address" className="form-control" placeholder="Nhập địa chỉ"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Mã giảm giá</label>
                        <div className="col-12">
                            <input type="text" id="inp-discount" className="form-control" placeholder="Nhập mã giảm giá"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Lời nhắn</label>
                        <div className="col-12">
                            <input type="text" id="inp-message" className="form-control" placeholder="Nhập lời nhắn"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <button className="btn font-weight-bold col-12" id="btn-order-detail"
                            style={{backgroundColor: "orange"}}>Gửi</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default FormComponent
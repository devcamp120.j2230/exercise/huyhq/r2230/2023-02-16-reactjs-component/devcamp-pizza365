import { Component } from "react";

class DrinkComponent extends Component{
    render(){
        return(
            <div className="mt-5">
                <div className="text-warning text-center mb-3">
                    <span className="h3 border-bottom border-warning">Chọn đồ uống</span>
                </div>
                <div className="m-5">
                    <select name="" id="select-drink" className="form-control form-control-md col-12 m-6"
                        aria-label="Default select example">
                        <option value="0">Tất cả các loại nước uống...</option>
                    </select>
                </div>
            </div>
        )
    }
}

export default DrinkComponent;

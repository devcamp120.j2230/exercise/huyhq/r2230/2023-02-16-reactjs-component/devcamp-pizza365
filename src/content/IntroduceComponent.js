import { Component } from "react";

class IntroduceComponent extends Component{
    render(){
        return(
            <div className="mt-5">
          <div className="text-warning text-center mb-3">
            <span className="h3 border-bottom border-warning"> Tại sao lại Pizza 365</span>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-3 p-5 border border-warning"
              style={{ backgroundColor: "lightgoldenrodyellow" }}>
              <h4 className="mb-3">Đa dạng</h4>
              <span>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</span>
            </div>
            <div className="col-sm-12 col-md-3 p-5 border border-warning" style={{ backgroundColor: "yellow" }}>
              <h4 className="mb-3">Chất lượng</h4>
              <span>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực
                phẩm.</span>
            </div>
            <div className="col-sm-12 col-md-3 p-5 border border-warning" style={{ backgroundColor: "lightsalmon" }}>
              <h4 className="mb-3">Hương vị</h4>
              <span>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</span>
            </div>
            <div className="col-sm-12 col-md-3 p-5 border border-warning" style={{ backgroundColor: "orange" }}>
              <h4 className="mb-3">Dịch vụ</h4>
              <span>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân
                tiến.</span>
            </div>
          </div>
        </div>
        )
    }
};

export default IntroduceComponent;
import { Component } from "react";
import img1 from '../asset/images/1.jpg';
import img2 from '../asset/images/2.jpg';
import img3 from '../asset/images/3.jpg';
import img4 from '../asset/images/4.jpg';
import FormComponent from './FormComponent';
import DrinkComponent from './DrinkComponent';
import SizeComponent from './SizeComponent';
import TypeComponent from './TypeComponent';
import IntroduceComponent from './IntroduceComponent';
class ContentComponent extends Component {
    render() {
        return (
            <div className="container mt-5">
                <div className="mb-3">
                    <h1 className="text-warning">PIZZA 365 </h1>
                    <span className="text-warning font-italic">Truly italian!</span>
                </div>
                {/* <!-- SLIDE --> */}
                <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                    <ol className="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="d-block w-100" src={img1} alt="First slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={img2} alt="Second slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={img3} alt="Third slide" />
                        </div>
                        <div className="carousel-item">
                            <img className="d-block w-100" src={img4} alt="Fourth slide" />
                        </div>
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
                {/* Thông tin cửa hàng */}
                <IntroduceComponent />
                {/* <!-- INFOMATION ORDER--> */}
                <div className=''>
                    {/* <!-- Chọn Size Pizza --> */}
                    <SizeComponent />
                    {/* <!-- Chọn loại Pizza --> */}
                    <TypeComponent />
                    {/* <!-- Chọn loại đồ uống --> */}
                    <DrinkComponent />
                    {/* <!-- Thông tin Order --> */}
                    <FormComponent />
                </div>
            </div>
        )
    }
}

export default ContentComponent;
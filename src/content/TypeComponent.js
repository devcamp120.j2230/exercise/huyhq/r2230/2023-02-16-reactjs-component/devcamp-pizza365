import { Component } from "react";
import imgSeafoot from '../asset/images/seafood.jpg';
import imgHawaiian from '../asset/images/hawaiian.jpg';
import imgBacon from '../asset/images/bacon.jpg';

class TypeComponent extends Component{
    render(){
        return(
            <div className="mt-5" id="type-pizza-choose">
                    <div className="text-warning text-center mb-3">
                        <span className="h3 border-bottom border-warning">Chọn loại pizza</span>
                    </div>
                    <div className="row justify-content-center">
                        <div className="row justify-content-center col-sm-12 col-md-4 p-3">
                            <div className="card" style={{ width: "18rem" }}>
                                <img className="card-img-top" src={imgSeafoot} alt="Card image cap" />
                                <div className="card-body">
                                    <div className="card-title">
                                        <p className="h5">OCEAN MANIA</p>
                                        <p className="card-text">PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                    </div>
                                    <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Tôm Mực, Thanh Cua, Hành Tây.</p>
                                    <div className="btn col-12 type-pizza-btn" style={{ backgroundColor: "orange" }}
                                        data-is-selected-type-pizza="N" id="type-pizza-seafood">Chọn</div>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-center col-sm-12 col-md-4 p-3">
                            <div className="card" style={{ width: "18rem" }}>
                                <img className="card-img-top" src={imgHawaiian} alt="Card image cap" />
                                <div className="card-body">
                                    <div className="card-title">
                                        <p className="h5">HAWAIIAN</p>
                                        <p className="card-text">PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                    </div>
                                    <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                                    <div className="btn col-12 type-pizza-btn" style={{ backgroundColor: "orange" }}
                                        data-is-selected-type-pizza="N" id="type-pizza-hawaii">Chọn</div>
                                </div>
                            </div>
                        </div>
                        <div className="row justify-content-center col-sm-12 col-md-4 p-3">
                            <div className="card" style={{ width: "18rem" }}>
                                <img className="card-img-top" src={imgBacon} alt="Card image cap" />
                                <div className="card-body">
                                    <div className="card-title">
                                        <p className="h5">CHEESY CHICKEN BACON</p>
                                        <p className="card-text">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                    </div>
                                    <p className="card-text">Xốt Cà Chua, Thịt gà, Thịt Heo Muối, Phô Mai Mozzarella.</p>
                                    <div className="btn col-12 type-pizza-btn" style={{ backgroundColor: "orange" }}
                                        data-is-selected-type-pizza="N" id="type-pizza-bacon">Chọn</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

export default TypeComponent;